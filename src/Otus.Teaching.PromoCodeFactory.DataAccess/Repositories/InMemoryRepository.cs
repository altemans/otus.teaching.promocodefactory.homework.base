﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            if (id == null)
                throw new ArgumentNullException();
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task Create(T item)
        {
            try
            {
                if(item == null)
                    throw new ArgumentNullException();
                Data.Add(item);
                await Task.CompletedTask;
            }
            catch(Exception ex)
            {
                await Task.FromException(ex);
            }
        }

        public async Task Update(T item) 
        {
            try
            {
                var testItem = Data.FirstOrDefault(x => x.Id == item.Id);
                if (testItem == null)
                    throw new KeyNotFoundException();
                var changeItem = Data.IndexOf(Data.FirstOrDefault(x => x.Id == item.Id));
                Data[changeItem] = item;
                await Task.CompletedTask;
            }
            catch(Exception ex)
            {
                await Task.FromException(ex);
            }
        }
        public async Task Delete(Guid id)
        {
            try
            {
                var deleteItem = Data.FirstOrDefault(x => x.Id == id);
                if (deleteItem == null)
                    throw new KeyNotFoundException();
                Data.Remove(deleteItem);
                await Task.CompletedTask;
            }
            catch(Exception ex)
            {
                await Task.FromException(ex);
            }
        }

    }
}